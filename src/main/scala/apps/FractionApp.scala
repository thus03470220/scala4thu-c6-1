package apps

import cc.Fraction

/**
  * Created by mac005 on 2017/4/24.
  */
object FractionApp extends App{
  val frac=Fraction(1,2)+ Fraction(1,3)
  println(frac)

  val frac2=Fraction(1,3)==Fraction(2,6).reduce()
  println(frac2)

  val frac3=Fraction(1,3) equal Fraction(2,4)
  println(frac3)

  val frac4=Fraction(3,0)
  println(frac4)


}
